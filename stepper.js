const nextBtn = document.querySelector(".stepper #nextBtn");
const previousBtn = document.querySelector(".stepper #previousBtn");
const steps = Array.prototype.slice.call(document.querySelectorAll(".stepper .stepper-header .step"));
const stepsContent = Array.prototype.slice.call(document.querySelectorAll(".stepper .stepper-content .step-content"));

if (steps.length != stepsContent.length) {
    throw new Error(`La cantidad de pasos (${steps.length}) no es igual a la cantidad de contenidos (${stepsContent.length}) o viceversa. ¡Verifica!`);
}

nextBtn.addEventListener("click", () => StepeerMoveStep("next"));
previousBtn.addEventListener("click", () => StepeerMoveStep("previous"));

const StepeerMoveStep = (option) => {
    const currentActive = document.querySelector(".stepper .step.active");
    const currentActiveIndex = steps.indexOf(currentActive);
    
    switch (option) {
        case "next":
            let indexToChange = currentActiveIndex + 1;
            
            if (indexToChange < steps.length) {
                currentActive.classList.remove("active");
                steps[currentActiveIndex].classList.add("passed");
                steps[indexToChange].classList.add("active");
                stepsContent.map(content => content.classList.remove("active"));
                stepsContent[indexToChange].classList.add("active");
                if (previousBtn.disabled) previousBtn.disabled = false;
                if (indexToChange + 1 == steps.length) nextBtn.disabled = true;
            }
            break;

        case "previous":
            if (currentActiveIndex >= 1) {
                let indexToChange = currentActiveIndex - 1;
                currentActive.classList.remove("active");
                steps[indexToChange].classList.remove("passed");
                steps[indexToChange].classList.add("active");
                stepsContent.map(content => content.classList.remove("active"));
                stepsContent[indexToChange].classList.add("active");
                if (indexToChange == 0) previousBtn.disabled = true; nextBtn.disabled = false;
            }
            break;
            
        default:
            break;
    }
};
